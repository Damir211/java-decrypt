package kz.damir021;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI extends JFrame {
    private JButton button2 = new JButton("Узнать метод шифрования");
    private JTextArea input = new JTextArea();
    private JTextArea input2 = new JTextArea();
    private JLabel label = new JLabel("Зашифрованный текст:");
    private JLabel label2 = new JLabel("Не зашифрованный текст:");
    private JLabel label3 = new JLabel("Метод шифрования:");
    private JLabel label4 = new JLabel("");
    private JPanel mainPannel = new JPanel();
    private JPanel textAreaPannel = new JPanel();
    private JPanel textAreaPannel2 = new JPanel();
    private JPanel buttonsPannelAll = new JPanel();
    private JPanel buttonsPannel = new JPanel();
    private JPanel buttonsPannel2 = new JPanel();
    private JPanel labelsPannel = new JPanel();




    public GUI () {
        super("Узнать метод шифрования (Цезарь, Атбаш, A1Z26)");
        Image icon = new ImageIcon(this.getClass().getResource("/logo/logo.png")).getImage();
        this.setIconImage(icon);


        this.setBounds(100,100,500,500);
        this.setMinimumSize(new Dimension(500, 500));
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        input.setLineWrap(true);
        input.setWrapStyleWord(true);

        input2.setLineWrap(true);
        input2.setWrapStyleWord(true);

        button2.addActionListener(new DecryptEvent());
        mainPannel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        mainPannel.setLayout(new GridLayout(0,1));

        input.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        input2.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));


        textAreaPannel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        textAreaPannel.setLayout(new GridLayout(0,2));
        textAreaPannel.add(label);
        textAreaPannel.add(input);
        JScrollPane inputScroll = new JScrollPane(input, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        textAreaPannel.add(inputScroll);

        textAreaPannel2.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        textAreaPannel2.setLayout(new GridLayout(0,2));
        textAreaPannel2.add(label2);
        textAreaPannel2.add(input2);
        JScrollPane inputScroll2 = new JScrollPane(input2, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        textAreaPannel2.add(inputScroll2);

        labelsPannel.setBorder(BorderFactory.createEmptyBorder(10, 0, 10, 0));
        labelsPannel.setLayout(new GridLayout(0,2));
        labelsPannel.add(label3);
        labelsPannel.add(label4);


        buttonsPannel2.setBorder(BorderFactory.createEmptyBorder(5, 0, 5, 0));
        buttonsPannel2.setLayout(new GridLayout(1,1));

        buttonsPannelAll.setBorder(BorderFactory.createEmptyBorder(10, 0, 0, 0));
        buttonsPannelAll.setLayout(new GridLayout(2,2));


        buttonsPannel2.add(button2);

        buttonsPannelAll.add(labelsPannel);
        buttonsPannelAll.add(buttonsPannel2);


        mainPannel.add(textAreaPannel);
        mainPannel.add(textAreaPannel2);
        mainPannel.add(buttonsPannelAll);


        Container container = this.getContentPane();

        container.add(mainPannel);



    }

    Decrypt decryptMethods = new Decrypt();


    class DecryptEvent implements ActionListener {
        public void actionPerformed (ActionEvent e){
            String text = input.getText();
            String text2 = input2.getText();
            if(text.length() == 0){
                JOptionPane.showMessageDialog(null, "Введите текст", "Error", JOptionPane.PLAIN_MESSAGE);
                return;
            }
            String result = decryptMethods.checkText(text, text2);
            label4.setText(result);
        }
    }
}
