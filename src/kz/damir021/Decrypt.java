package kz.damir021;

public class Decrypt {
    public String outPutText = "";

    public int ruBigIndexStart = 1040;
    public int ruBigIndexEnd = 1071;

    public int ruIndexStart = 1072;
    public int ruIndexEnd = 1103;

    public int engBigIndexStart = 65;
    public int engBigIndexEnd = 90;

    public int engIndexStart = 97;
    public int engIndexEnd = 122;

    public boolean isCeasar(String inText, String outText) {
        char[] myArray = new char[inText.length()];
        for(int i = 1; i < 26; i++){
            outPutText = "";
            for (int j = 0; j < inText.length(); j++){
                char symbolChar = inText.charAt(j);
                int symbolNumber = (int)symbolChar;

                if(ruBigIndexStart <= symbolNumber && symbolNumber <= ruBigIndexEnd){
                    symbolNumber = shift(symbolNumber, i, ruBigIndexStart, ruBigIndexEnd);
                }else if(ruIndexStart <= symbolNumber && symbolNumber <= ruIndexEnd){
                    symbolNumber = shift(symbolNumber, i, ruIndexStart, ruIndexEnd);
                }else if(engBigIndexStart <= symbolNumber && symbolNumber <= engBigIndexEnd){
                    symbolNumber = shift(symbolNumber, i, engBigIndexStart, engBigIndexEnd);
                }else if(engIndexStart <= symbolNumber && symbolNumber <= engIndexEnd){
                    symbolNumber = shift(symbolNumber, i, engIndexStart, engIndexEnd);
                }
                outPutText += (char)symbolNumber;
            }
            if(outText.equals(outPutText)){
                return true;
            }
        }
        return false;
    }
    public boolean isAtbash(String inText, String outText) {
        char[] myArray = new char[inText.length()];
        outPutText = "";
        for (int j = 0; j < inText.length(); j++){
            char symbolChar = inText.charAt(j);
            int symbolNumber = (int)symbolChar;

            if(ruBigIndexStart <= symbolNumber && symbolNumber <= ruBigIndexEnd){
                symbolNumber = ruBigIndexEnd - symbolNumber + ruBigIndexStart;
            }else if(ruIndexStart <= symbolNumber && symbolNumber <= ruIndexEnd){
                symbolNumber = ruIndexEnd - symbolNumber + ruIndexStart;
            }else if(engBigIndexStart <= symbolNumber && symbolNumber <= engBigIndexEnd){
                symbolNumber = engBigIndexEnd - symbolNumber + engBigIndexStart;
            }else if(engIndexStart <= symbolNumber && symbolNumber <= engIndexEnd){
                symbolNumber = engIndexEnd - symbolNumber + engIndexStart;
            }

            outPutText += (char)symbolNumber;
        }
        if(outText.equals(outPutText)){
            return true;
        }
        return false;
    }
    public boolean isA1Z26(String inText, String outText) {
        char[] myArray = new char[inText.length()];
        String outTextLower = outText.toLowerCase();
        for(int i = 0; i < 2; i++){
            outPutText = "";
            String indexChar = "";
            for (int j = 0; j < inText.length(); j++){
                char symbolChar = inText.charAt(j);
                if(isNumeric(Character.toString(symbolChar))){
                    indexChar += symbolChar;
                    if(j == (inText.length() - 1)){
                        if(indexChar.length() > 0){
                            if(i == 0){
                                outPutText += (char)(Integer.parseInt(indexChar) + 1071);
                            }else{
                                outPutText += (char)(Integer.parseInt(indexChar) + 96);
                            }
                            indexChar = "";
                        }
                    }
                }else if(symbolChar == ' '){
                    if(indexChar.length() > 0){
                        if(i == 0){
                            outPutText += (char)(Integer.parseInt(indexChar) + 1071);
                        }else{
                            outPutText += (char)(Integer.parseInt(indexChar) + 96);
                        }
                        indexChar = "";
                    }
                    outPutText += " ";
                }else{
                    if(indexChar.length() > 0){
                        if(i == 0){
                            outPutText += (char)(Integer.parseInt(indexChar) + 1071);
                        }else{
                            outPutText += (char)(Integer.parseInt(indexChar) + 96);
                        }
                        indexChar = "";
                    }
                }
            }
            if(outTextLower.equals(outPutText)){
                return true;
            }
        }
        return false;
    }
    public String checkText(String inText, String outText){
        if(isCeasar(inText, outText)){
            return "Цезарь";
        }else if(isAtbash(inText, outText)){
            return "Атбаш";
        }else if(isA1Z26(inText, outText)){
            return "A1Z26";
        }else{
            return "Не известно";
        }
    }
    public int shift(int charNumber, int key, int startSymbol, int endSymbol){
        int charNumberIndex = charNumber;
        if((charNumberIndex - key) < startSymbol){
            charNumberIndex -= key;
            charNumberIndex += (endSymbol - startSymbol + 1);
        }else{
            charNumberIndex -= key;
        }
        return charNumberIndex;
    }
    public static boolean isNumeric(String strNum) {
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }
}
